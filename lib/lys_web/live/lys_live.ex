defmodule LysWeb.LysLive do
  use LysWeb, :live_view

  @topic "color-changes"

  def mount(_params, _, socket) do
    if connected? socket do
      LysWeb.Endpoint.subscribe @topic
    end
    {:ok, assign(socket, :color, "#ff0000")}
  end

  def render(assigns) do
    ~H"""
    <style>
      * {
        background-color: <%= @color %>;
      }
    </style>
    Current color: <%= @color %>
    <form phx-submit="color-change">
      <input name="color" type="text"/>
      <input type="submit"/>
    </form>
    """
  end

  def handle_event("color-change", %{"color" => color}, socket) do
    LysWeb.Endpoint.broadcast_from(self(), @topic, "color-change", color)
    {:noreply, assign(socket, :color, color)}
  end

  def handle_info(%{"event": "color-change", "payload": new_color}, socket) do
    {:noreply, assign(socket, :color, new_color)}
  end
end
